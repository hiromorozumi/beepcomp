BeepComp - Chiptune Creator
===========================

This software assists you in making music that sounds like 8-bit game consoles or old PCs.
The latest release is always available at the SourceForge project top page:
http://beepcomp.sourceforge.net